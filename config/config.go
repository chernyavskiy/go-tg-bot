package config

import (
	"encoding/json"
	"fmt"
	"os"
)

//Config struct implement secret keys for app
type Config struct {
	BotKey string `json:"bot_key"`
	APIKey string `json:"api_key"`
}

//GetConfigData implement reading congif file and return config struct
func GetConfigData(path string) (config Config) {
	file, err := os.Open(path)
	defer file.Close()
	if err != nil {
		fmt.Printf("%v", err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		fmt.Printf("%v", err)
	}
	return
}
