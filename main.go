package main

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"log"

	"./config"
	"./darksky"
)

var configPath = "./config.json"
var configData = config.GetConfigData(configPath)
var url = "https://api.darksky.net/forecast/"
var lat = "54.980593"
var long = "82.804049"
var queryParams = darksky.Params{
	URL:     url,
	Lat:     lat,
	Long:    long,
	Units:   "si",
	Lang:    "ru",
	Key:     configData.APIKey,
	Exclude: []string{"minutely", "alerts", "flags"},
}

func main() {
	bot, err := tgbotapi.NewBotAPI(configData.BotKey)
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true
	log.Printf("Authorized on account %s", bot.Self.UserName)

	var ucfg = tgbotapi.NewUpdate(0)
	ucfg.Timeout = 60
	updates, err := bot.GetUpdatesChan(ucfg)
	if err != nil {
		log.Printf("%v", err)
	}

	for update := range updates {
		if update.Message == nil {
			continue
		}

		if update.Message.IsCommand() {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")
			switch update.Message.Command() {
			case "forecast":
				result, err := darksky.GetData(queryParams)
				if err != nil {
					log.Printf("%v", err)
				}
				msg.Text = darksky.GetMessageText(result)
			case "daily":
				params := queryParams
				params.Exclude = append(params.Exclude, "hourly")
				result, err := darksky.GetData(params)
				if err != nil {
					log.Printf("%v", err)
				}

				msg.Text = darksky.GetMessageText(result)
			case "hourly":
				params := queryParams
				params.Exclude = append(params.Exclude, "daily")
				result, err := darksky.GetData(params)
				if err != nil {
					log.Printf("%v", err)
				}
				msg.Text = darksky.GetMessageText(result)
			default:
				msg.Text = "I don't know this command"
			}
			bot.Send(msg)
		}
	}
}
