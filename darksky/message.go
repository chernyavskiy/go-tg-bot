package darksky

import (
	"bytes"
	"log"
	"text/template"
	"time"
)

func timeFormat(sec int64) string {
	date := time.Unix(sec, 0)
	date2str := date.Format("15:04")
	return date2str
}

func dateFormat(sec int64) string {
	date := time.Unix(sec, 0)
	date2str := date.Format("2 Jan")
	return date2str
}

func getPrecipIcon(precipType string) (result string) {
	if precipType == "sleet" {
		result = string(fallout["rain"]) + string(fallout["snow"])
	} else {
		result = string(fallout[precipType])
	}
	return
}
func getUTF(icon string) rune {
	value, ok := icons[icon]
	if !ok {
		value = '\U00002754'
	}
	return value
}

func prob2bool(prob float32) bool {
	return prob > 0.1
}

var fallout = map[string]rune{
	"rain": '\U0001F42A',
	"snow": '\U00002744',
}

var icons = map[string]rune{
	"clear-day":           '\U00002600',
	"clear-night":         '\U00002600',
	"rain":                '\U0001F329',
	"snow":                '\U0001F328',
	"sleet":               '\U0001F328',
	"wind":                '\U0001F32C',
	"fog":                 '\U0001F32B',
	"cloudy":              '\U00002601',
	"partly-cloudy-day":   '\U000026C5',
	"partly-cloudy-night": '\U000026C5',
	//"hail"
	//"thunderstorm"
	//"tornado"
}

//GetMessageText parse data and return text message.
func GetMessageText(weather *Weather) string {
	t := template.New("Forecast")
	text := `Currently data
Temperature: {{.Currently.Temperature}}
Apparent: {{.Currently.ApparentTemperature}}
Summary: {{.Currently.Summary}}
Time: {{.Currently.Time | unix2time }}
Forecast
{{ if .Hourly.Data }}{{"Time" | printf "%10s"}}{{"Temperature" | printf "%15s"}}
{{range $v := .Hourly.Data}}{{$v.Time | unix2time | printf "%10s"}}{{$v.Temperature | printf "%15.2f"}}{{" "|printf "%8s"}}{{$v.Icon | getUTF | printf "%c"}}{{if $v.PrecipProbablity | prob2bool}}  {{$v.PrecipType| getPrecipIcon}}, {{$v.PrecipProbablity}}{{end}}
{{end}}
{{ else }}Date   Min temp.   Max temp.
{{range $v := .Daily.Data}}{{$v.Time | unix2date}}     {{$v.TemperatureMin}}    {{$v.TemperatureMax}}   {{$v.Icon|getUTF|printf "%c"}}
{{end}}
{{ end }}
`
	t.Funcs(template.FuncMap{"unix2time": timeFormat,
		"unix2date":     dateFormat,
		"getPrecipIcon": getPrecipIcon,
		"getUTF":        getUTF,
		"prob2bool":     prob2bool})
	t, err := t.Parse(text)
	if err != nil {
		log.Printf("%v", err)
	}
	var tmpl bytes.Buffer
	if err = t.Execute(&tmpl, weather); err != nil {
		log.Printf("%v", err)
	}
	return tmpl.String()
}
