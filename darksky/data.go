package darksky

//DPO contains various properties, each representing the average of a particular weather phenomenon occurring during a period of time.
type DPO struct {
	ApparentTemperature         float32 `json:",omitempty"`
	ApparentTemperatureHigh     float32 `json:",omitempty"`
	ApparentTemperatureHighTime int64   `json:",omitempty"`
	ApparentTemperatureLow      float32 `json:",omitempty"`
	ApparentTemperatureLowTime  int64   `json:",omitempty"`
	ApparentTemperatureMax      float32 `json:",omitempty"`
	ApparentTemperatureMaxTime  int64   `json:",omitempty"`
	ApparentTemperatureMin      float32 `json:",omitempty"`
	ApparentTemperatureMinTime  int64   `json:",omitempty"`
	CloudCover                  float32 `json:",omitempty"`
	DewPoint                    float32 `json:",omitempty"`
	Humidity                    float32 `json:",omitempty"`
	Icon                        string  `json:",omitempty"`
	MoonPhase                   float32 `json:",omitempty"`
	NearestStormBearing         int     `json:",omitempty"`
	NearestStormDistance        float32 `json:",omitempty"`
	Ozone                       float32 `json:",omitempty"`
	PrecipAccumulation          float32 `json:",omitempty"`
	PrecipIntensity             float32 `json:",omitempty"`
	PrecipIntensityError        float32 `json:",omitempty"`
	PrecipIntensityMax          float32 `json:",omitempty"`
	PrecipIntensityMaxTime      float32 `json:",omitempty"`
	PrecipProbablity            float32 `json:",omitempty"`
	PrecipType                  string  `json:",omitempty"`
	Pressure                    float32 `json:",omitempty"`
	Summary                     string  `json:",omitempty"`
	SunriseTime                 int64   `json:",omitempty"`
	SunsetTime                  int64   `json:",omitempty"`
	Temperature                 float32 `json:",omitempty"`
	TemperatureHigh             float32 `json:",omitempty"`
	TemperatureHighTime         int64   `json:",omitempty"`
	TemperatureLow              float32 `json:",omitempty"`
	TemperatureLowTime          int64   `json:",omitempty"`
	TemperatureMax              float32 `json:",omitempty"`
	TemperatureMaxTime          int64   `json:",omitempty"`
	TemperatureMin              float32 `json:",omitempty"`
	TemperatureMinTime          int64   `json:",omitempty"`
	Time                        int64   `json:",omitempty"`
	UvIndex                     int     `json:",omitempty"`
	UvIndexTime                 int64   `json:",omitempty"`
	Visibility                  float32 `json:",omitempty"`
	WindBearing                 int     `json:",omitempty"`
	WindGust                    float32 `json:",omitempty"`
	WindGustTime                int64   `json:",omitempty"`
	WindSpeed                   float32 `json:",omitempty"`
}

//DataBlock represents the various weather phenomena occurring over a period of time.
type DataBlock struct {
	Data    []DPO
	Summary string `json:",omitempty"`
	Icon    string `json:",omitempty"`
}

//AlertsArray contains objects representing the severe weather warnings issued for the requested location by a governmental authority.
type AlertsArray struct {
	Description string   `json:",omitempty"`
	Expires     int      `json:",omitempty"`
	Regions     []string `json:",omitempty"`
	Severity    string   `json:",omitempty"`
	Time        int64    `json:",omitempty"`
	Title       string   `json:",omitempty"`
	URI         string   `json:",omitempty"`
}

//FlagsObject contains various metadata information related to the request.
type FlagsObject struct {
	DarkskyUnavailable string   `json:"darksky-unavailable,omitempty"`
	NearestStation     float32  `json:"nearest-station,omitempty"`
	Sources            []string `json:",omitempty"`
	Units              string   `json:",omitempty"`
}

//Weather implement json data structure.
type Weather struct {
	Latitude  float32     `json:",omitempty"`
	Longitude float32     `json:",omitempty"`
	Timezone  string      `json:",omitempty"`
	Currently DPO         `json:",omitempty"`
	Minutely  DataBlock   `json:",omitempty"`
	Hourly    DataBlock   `json:",omitempty"`
	Daily     DataBlock   `json:",omitempty"`
	Alerts    AlertsArray `json:",omitempty"`
	Flags     FlagsObject `json:",omitempty"`
}
