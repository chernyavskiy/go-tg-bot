package darksky

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

//Params - query parameters for GetData function.
type Params struct {
	URL     string
	Key     string
	Lat     string
	Long    string
	Units   string
	Lang    string
	Exclude []string
}

//GetData - make request for DarkSky service.
func GetData(params Params) (result *Weather, err error) {
	q := params.URL + params.Key + "/" + params.Lat + "," + params.Long + "?lang=" + params.Lang + "&units=" + params.Units
	if params.Exclude != nil {
		q = q + "&exclude=" + strings.Join(params.Exclude, ",")
	}
	resp, err := http.Get(q)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Query error: %s", resp.Status)
	}
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return nil, err
	}
	return
}
